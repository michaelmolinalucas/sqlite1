package com.example.appre;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class recibir_parametro extends AppCompatActivity {
    TextView txtrp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibir_parametro);

        txtrp = (TextView) findViewById(R.id.txtrp);
        Bundle bundle = this.getIntent().getExtras();
        txtrp.setText(bundle.getString("dato"));

    }
}
