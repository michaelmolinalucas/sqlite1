package com.example.appre;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class pasar_parametro extends AppCompatActivity {
    Button btnpp;
    EditText edtpp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasar_parametro);
        btnpp = (Button) findViewById(R.id.btnpp);
        edtpp = (EditText) findViewById(R.id.edtpp);
        btnpp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(pasar_parametro.this,recibir_parametro.class);

                Bundle bundle = new Bundle();
                bundle.putString("dato", edtpp.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

    }
}
