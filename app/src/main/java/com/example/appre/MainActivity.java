package com.example.appre;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button buttonGuardar, buttonBuscar, buttonLogin, buttonPasar;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            buttonBuscar = (Button) findViewById(R.id.buttonBuscar);
            buttonGuardar = (Button) findViewById(R.id.buttonGuardar);
            buttonLogin = (Button) findViewById(R.id.buttonLogin);
            buttonPasar = (Button) findViewById(R.id.buttonPasar);

            buttonLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this,Ingresar.class);
                    startActivity(intent);
                }
            });

            buttonGuardar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this,GuardarActivity.class);
                    startActivity(intent);
                }
            });

            buttonBuscar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this,BuscarActivity.class);
                    startActivity(intent);
                }
            });

            buttonPasar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this,pasar_parametro.class);
                    startActivity(intent);
                }
            });


        }
}
